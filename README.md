# 🧮 Mathematica Infinita

Mathematica Infinita - Infinite Mathematics

# ℹ About
Mathematica Infinita is my repository which contains my observations and their results on Mathematics. This 
repo contains programs written for proofs and for verification. Most of these programs are written in [Julia language](https://julialang.org/)

# 🛠 Usage
To run the programs in this repository, The following specs are needed:
    - The `Julia` Programming Language
    - A computer with atleast 4 GB RAM

Just clone this repository and run the scripts using `Julia` Compiler.