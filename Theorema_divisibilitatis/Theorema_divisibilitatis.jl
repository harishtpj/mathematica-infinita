# Program to test numbers which satisfy the "Theorema Divisibilitatis"

# Theorema Divisibilitatis states that:
# If `n²` is divisible by `a`, then n is divisible by `a`
# for,
#   `n` ∈ ℕ and
#   `a` ∈ { `x` ∈ ℕ : prime factorisation of `x` has no repeated factors }

# Function to compute prime factors of a given number
function prime_factors(number)
    factors = Int[]
    d = 2

    while d * d <= number
        if number % d == 0
            push!(factors, d)
            number = number ÷ d
        else
            d += 1
        end
    end

    if number > 1
        push!(factors, number)
    end

    return factors
end

# Function to check if prime factorisation of `number` has no repeated factors
function are_factors_distinct(number)
    factors = prime_factors(number)
    for n in factors
        count = sum(factors .== n)
        if count > 1
            return false
        end
    end
    return true
end

# Generates a list of natural numbers until `to` which
# belongs to the specified set
function non_rep_prime_list(to)
    lst = Int[]
    for n in 2:to
        if are_factors_distinct(n)
            push!(lst, n)
        end
    end
    return lst
end

# Main Program
to_num = parse(Int, ARGS[1])
output_file = ARGS[2]
if to_num <= 2
    error("to_num should be greater than 2")
end

# Check if output file exists, create it if it doesn't exist
if !isfile(output_file)
    open(output_file, "w") do file
        # create an empty file
    end
end

# Initialisation of basic sets
set_n = 2:to_num
set_a = non_rep_prime_list(to_num)

open(output_file, "w") do file
    for n in set_n
        for a in set_a
            if n != a
                if (n^2 % a) == 0
                    println("$(n)^2 is divisible by $(a)")
                    println(file, "$(n)^2 is divisible by $(a)")
                    if (n % a) == 0
                        println("$(n) is divisible by $(a)\n")
                        println(file, "$(n) is divisible by $(a)\n")
                    else
                        println("$(n) is NOT divisible by $(a)\n")
                        println(file, "$(n) is NOT divisible by $(a)\n")
                    end
                end
            end
        end
    end
end