% General Geometry related theorems

\subsection{The Mgensec formula}
\begin{description}
    \item[S.no:] 6
    \item[Date:] \date{18-08-2022}
    \item[Code:] \texttt{G10 MAT 12 FML}
    \item[Concept] \hfill \\ Area of segment
    \item[Author] \hfill \\ Harish Kumar
\end{description}

\begin{fml}
    Generalised formula for area of Minor segment in circle is:
    \begin{displaymath}
        \frac{r^2}{2} \Bigg[\frac{\pi \theta}{180} - \sin \theta\Bigg]
    \end{displaymath}
\end{fml}

\begin{proof}
    Let us take a circle with centre $O$ and radius $r$ in which chord $AB$ subtend an angle $\theta$
    forming $\triangle OAB$.

    \begin{figure}[H]
        \begin{tikzpicture}
            \tkzDefPoints{0/0/O, -2/-2/A, 2/-2/B, 0/-2.8/C}
            \tkzDrawCircle(O,A)
            \tkzDrawSegment(O,A)
            \tkzDrawSegment(O,B)
            \tkzDrawSegment(A,B)
            \tkzDrawPoints(O,A,B,C)
            \tkzLabelPoints[above](O)
            \tkzLabelPoints[left](A)
            \tkzLabelPoints[right](B)
            \tkzLabelPoints[below](C)
            \tkzMarkAngle[size=0.5](A,O,B)
            \tkzLabelAngle[below, pos=0.5](A,O,B){$\theta$}
            \tkzLabelSegment[above](O,A){$r$}
            \tkzLabelSegment[above](O,B){$r$}
    
            \tkzDefPoints{7/0/A, 5/-2/O, 9/-2/B, 7/-2/D}
            \tkzDrawPolygon(O,A,B)
            \tkzDrawSegment(A,D)
            \tkzDrawPoints(O,A,B,D)
            \tkzLabelPoints[above](A)
            \tkzLabelPoints(O,B,D)
            \tkzMarkAngle[size=0.5](B,O,A)
            \tkzLabelAngle[pos=0.5, right](B,O,A){$\theta$}
            \tkzMarkRightAngle(O,D,A)
            \tkzLabelSegment[above](O,A){$r$}
            \tkzLabelSegment[below=12pt,left](O,B){$r$}
        \end{tikzpicture}
    
        \caption{Diagram representing Circle with $\triangle OAB$}
        \label{fig:mgensec_diag}
    \end{figure}

    \begin{align}
        ar(\triangle OAB) &= \frac{1}{2} \times b \times h \tag*{}\\
        &= \frac{1}{2} \times r \times AD \tag*{}\\
        &= \frac{1}{2} \times r \times r \times \frac{AD}{r} \tag*{}\\
        &= \frac{1}{2} \times r \times r \times \sin \theta \tag*{}\\
        &= \frac{r^2}{2} \times \sin \theta \tag*{}
    \end{align}

    Having found the area of triangle, we can now find the area of minor sector:
    \begin{align}
        ar(Segment ACB) &= ar(Sector OACB) - ar(\triangle OAB) \tag*{}\\
        &= \frac{\theta}{360} \times \pi r^2 - \frac{r^2}{2} \times \sin \theta \tag*{}\\
        &= \frac{r^2}{2} \times \frac{\pi \theta}{180} - \frac{r^2}{2} \times \sin \theta \tag*{}\\
        &= \frac{r^2}{2} \Bigg[\frac{\pi \theta}{180} - \sin \theta\Bigg] \tag*{}
    \end{align}
\end{proof}

\subsection{Proof for $\pi r^2$ by Integration}
\begin{description}
    \item[S.no:] 8
    \item[Date:] \date{19-08-2022}
    \item[Code:] \texttt{GEN MAT XX PRF}
    \item[Concept] \hfill \\ Area of circle
    \item[Author] \hfill \\ Harish Kumar
\end{description}

\begin{proof}
    Representing a circle in graph leads to following observations:
    \begin{itemize}
        \item $r$ lies on $x$-axis.
        \item Integrating circumference of circle along 0 to $r$($dx$) gives us area of circle.
    \end{itemize}

    \begin{align}
        \int_{0}^{r} 2\pi x \,dx &= 2\pi \int_{0}^{r} x \,dx \tag*{}\\
        &= 2\pi \Bigg[\frac{x^2}{2}\Bigg]_{0}^{r} \tag*{}\\
        &= 2\pi \Bigg[\frac{r^2}{2} - \frac{0}{2}\Bigg] \tag*{}\\
        &= 2\pi \times \frac{r^2}{2} \tag*{}\\
        &= \pi r^2
    \end{align}
\end{proof}

\subsection{The SAS \textit{'ar'} formula}
\begin{description}
    \item[S.no:] 12
    \item[Date:] \date{02-06-2022}
    \item[Code:] \texttt{G10 MAT 06 FML (DRV)}
    \item[Concept] \hfill \\ Area of triangle
    \item[Author] \hfill \\ Harish Kumar
\end{description}

\begin{fml}
    Area of a triangle with two sides and an included angle can be found as follows:
    \begin{displaymath}
        ar(\triangle ABC) =  \frac{ab\sin \theta}{2}
    \end{displaymath}
    Where:
    \begin{itemize}
        \item $a$ and $b$ are the sides of the triangle.
        \item $\theta$ is the included angle between $a$ and $b$.
    \end{itemize}
\end{fml}

\begin{proof}
    Let us consider a triangle $ABC$ with $AD \perp BC$.

    \begin{figure}[H]
        \begin{tikzpicture}
            \tkzDefPoints{0/0/A, -3/-4/B,  2/-4/C, 0/-4/D}
            \tkzDrawPolygon(A,B,C)
            \tkzDrawSegment(A,D)
            \tkzDrawPoints(A,B,C,D)
            \tkzLabelPoints[above](A)
            \tkzLabelPoints[below](B,C,D)
            \tkzLabelSegment[left](A,B){$a$}
            \tkzLabelSegment[below](B,C){$b$}

            \tkzMarkRightAngle(A,D,B)
            \tkzMarkAngle[size=0.5](C,B,A)
            \tkzLabelAngle[pos=0.5, right](C,B,A){$\theta$}
        \end{tikzpicture}
    
        \caption{Diagram representing Triangle $\triangle ABC$}
        \label{fig:sasar_diag}
    \end{figure}

    \begin{align}
        ar(\triangle ABC) &= \frac{1}{2} \times BC \times AD \tag*{}\\
        &= \frac{1}{2} \times b \times AD \tag*{}\\
        &= \frac{1}{2} \times b \times \sin \theta \times a \tag*{}\\
        &= \frac{ab\sin \theta}{2} \tag*{}\\
    \end{align}
\end{proof}
